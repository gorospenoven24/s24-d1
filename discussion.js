// (Comaprison) query operators 

// $gt(greater than) /$gte(greater than or equal to)

// syntax:
// db.collectionName.find({field: {$gt: value}});
// db.collectionName.find({field: {$gte: value}});

db.users.find({"age": {$gt: 50}});
db.users.find({"age": {$gte: 70}});



// $lt(less than) $lte(less that or equal to)
// syntax:
// db.collectionName.find({field: {$lt: value}});
// db.collectionName.find({field: {$lte: value}});
db.users.find({"age": {$lt: 50}});
db.users.find({"age": {$lte: 50}});



// $ne(not equal)
// syntax:
// db.collectionName.find({field: {$ne: value}});
db.users.find({"age": {$ne: 50}});


// $in
// syntax :
	// db.collectionName.find({field: {$in: [value....]}});

db.users.find({"lastName": {$in:["Hawking", "gorospe"]}});


// (logical )query oprator

  // $or ----------atlis 1 is true
  //  syntax: atlis 1 true
  	//  db.colectionName.find({$or: [{fieldA:valueA}, {fieldB: valueB}]});
db.users.find({
	 $or: [{ 
	 	"firstName": "Neil" 
	 	},
	 	{ "age": 
	 	{ $gt: 25} 
	 }] });




 //$and ----------------both condition must be true
  //syntax: All true
	//  db.colectionName.find({$or: [{fieldA:valueA}, {fieldB: valueB}]});

	db.users.find({
		$and: [
		{
			"age":{
				$ne: 82
			}
		},
		{
			"age": {
				$ne:76
			}
		}]
	});



//  field projections --by default _id is set 1 in field projection
//  inclusion
// fields are included to resulting documents
// syntax: 
// db.collectioMethod.find({"field ": value},{"field": 1, [ field: 1, ...]});


db.users.find(
	{"firstName ": {$ne : "Jane"}},
	{"firstName": 1,
		"lastName":1}
	);





// Exlclusion
// fileds are excluded to the resulting documents.
// syntax: 
// db.collectioMethod.find({"field" : value},{"field": 0, [field: 1, ...]});


db.users.find(
	{"firstName": {$ne: "John"}},
	{"_id": 0,
	 "contact": 0,
	 "department" : 0}
	);



// inclusion and exclusion of nested document fields
//syntax for inlusion:
	/*
	db.collectionName.find({
	   field: value },
	{
	   field.nestedField: 1, [field.nestedField: 1, ...]
	});
	*/

db.users.find(
	{"age":{$gt : 75}},

	{"firstName": 1,
		"lastName" : 1,
	   "contact.phone": 1}
	   );






// syntax for exclusion:

	/*
	db.collectionName.find({
	   field: value },
	{
	   field.nestedField: 0, [field.nestedField: 0, ...]
	});
	*/




	//  Evaluation query operator
	// $regex -----patterns

	// https://regexone.com/ ---------------link tutorial
	// syntax:
	/*
	db.collection.find({field:$regex: 'pattern', $options: 'optionValue'});
	*/

	
	db.users.find(
		{"firstName": {$regex:"n" ,$options: "$i"}}
		);



	// end for queries